//
//  InfoAboutBattleViewController.swift
//  BattleField
//
//  Created by Андрей on 17.05.17.
//
//

import UIKit

class InfoAboutBattleViewController: UIViewController {

    
    @IBOutlet weak var textView: UITextView!
    var lineView:UIImageView!
    var views:[UIView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(image: UIImage(named: "back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(NotificationsTableViewController.back(sender:)))
        
        self.navigationItem.leftBarButtonItem = newBackButton
        let lineFrame = CGRect(x: (self.navigationController?.navigationBar.frame.width)!/2 - 10, y: 30, width: 20, height: 8)
        
        let lineImage = UIImage(named: "greenUnderline")
        lineView = UIImageView(frame: lineFrame)
        lineView.image = lineImage
        self.navigationController?.navigationBar.addSubview(lineView)
        
        self.navigationController?.navigationBar.tintColor = UIColor(red: 105.0/255.0, green: 153.0/255.0, blue: 93.0/255.0, alpha: 1.0)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
        self.navigationItem.title = "О мероприятии"
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 13.0)!,NSForegroundColorAttributeName: UIColor.black]
        
        textView.font = UIFont(name: "HelveticaNeue-Thin", size: 20)
        textView.textColor = UIColor(red: 50.0/255.0, green: 52.0/255.0, blue:50.0/255.0, alpha: 1.0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func back(sender: UIBarButtonItem) {
        self.lineView.isHidden = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
        _ = navigationController?.popViewController(animated: true)
    }
    
    
    
    func changeStyle(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named:"square"), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
    }

    
    override func viewWillAppear(_ animated: Bool) {
        views = self.navigationController?.navigationBar.subviews
        for view in (self.navigationController?.navigationBar.subviews)!{
            view.removeFromSuperview()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        for view in (views)!{
            self.navigationController?.navigationBar.addSubview(view)
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.changeStyle()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
