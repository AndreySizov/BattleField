//
//  SocialNetworkingTableViewCell.swift
//  BattleField
//
//  Created by Андрей on 12.05.17.
//
//

import UIKit

class SocialNetworkingTableViewCell: UITableViewCell {

    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var shotView: UIImageView!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()

        loginLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 23)
        loginLabel.textColor = UIColor.black
        
        timeLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 13)
        timeLabel.textColor = UIColor(red: 50.0/255.0, green: 52.0/255.0, blue: 50.0/255.0, alpha: 1.0)
        
        commentLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 16)
        commentLabel.textColor = UIColor(red: 50.0/255.0, green: 52.0/255.0, blue: 50.0/255.0, alpha: 1.0)
        

        
        shotView.image = UIImage(named: "photo")
        shotView.contentMode = .scaleAspectFit
        
        
        avatarView.image = UIImage(named: "avatar")
        avatarView.layer.cornerRadius = (UIImage(named: "avatar")?.size.height)! / 5
        avatarView.contentMode = .scaleAspectFill
        avatarView.clipsToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
