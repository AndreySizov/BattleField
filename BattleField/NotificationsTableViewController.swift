//
//  NotificationsTableViewController.swift
//  BattleField
//
//  Created by Андрей on 15.05.17.
//
//

import UIKit

class NotificationsTableViewController: UITableViewController {

    var lineView:UIImageView!
    var views:[UIView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(image: UIImage(named: "back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(NotificationsTableViewController.back(sender:)))

        let lineFrame = CGRect(x: (self.navigationController?.navigationBar.frame.width)!/2 - 10, y: 30, width: 20, height: 8)
        
        let lineImage = UIImage(named: "greenUnderline")
        lineView = UIImageView(frame: lineFrame)
        lineView.image = lineImage
        
        
        self.navigationController?.navigationBar.addSubview(lineView)
        
        
        
        self.navigationItem.leftBarButtonItem = newBackButton

        
        
        self.navigationController?.navigationBar.tintColor = UIColor(red: 105.0/255.0, green: 153.0/255.0, blue: 93.0/255.0, alpha: 1.0)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
        self.navigationItem.title = "Уведомления"
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 13.0)!,NSForegroundColorAttributeName: UIColor.black]

//        UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont(name: "Helvetica", size: 13)], for: .normal)
        
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 5
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notification", for: indexPath)

        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return 185
    }
    
    
    func back(sender: UIBarButtonItem) {
        self.lineView.isHidden = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
        _ = navigationController?.popViewController(animated: true)
    }
    
    
    
    func changeStyle(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named:"square"), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
    }
    
    override func viewWillAppear(_ animated: Bool) {
        views = self.navigationController?.navigationBar.subviews
        for view in (self.navigationController?.navigationBar.subviews)!{
            view.removeFromSuperview()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        for view in (views)!{
            self.navigationController?.navigationBar.addSubview(view)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.changeStyle()
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
