//
//  MapViewController.swift
//  BattleField
//
//  Created by Андрей on 12.05.17.
//
//

import UIKit

class MapViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    var lineView1:UIImageView!
    var lineView2:UIImageView!
    var mapButton:UIButton!
    var myWaysButton:UIButton!


    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.image = UIImage(named: "fullmap")
        
        let lineFrame = CGRect(x: (self.navigationController?.navigationBar.frame.width)!/2 - 65, y: 30, width: 20, height: 8)
        
        let lineImage = UIImage(named: "greenUnderline")
        lineView1 = UIImageView(frame: lineFrame)
        lineView1.image = lineImage
        self.navigationController?.navigationController?.navigationBar.addSubview(lineView1)
        
        mapButton = UIButton(frame: CGRect(x: (self.navigationController?.navigationBar.frame.width)!/2 - 75, y: 12, width: 40, height: 20))
        mapButton.setTitle("Карта", for: UIControlState())
        mapButton.setTitleColor(UIColor.black, for: UIControlState())
        mapButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 13)
        mapButton.addTarget(self, action: #selector(mapButtonClicked(_ :)), for: UIControlEvents.touchUpInside)
        self.navigationController?.navigationController?.navigationBar.addSubview(mapButton)
        
        
        let lineFrame2 = CGRect(x: (self.navigationController?.navigationBar.frame.width)!/2 + 30, y: 30, width: 20, height: 8)

        lineView2 = UIImageView(frame: lineFrame2)
        lineView2.image = lineImage
        self.navigationController?.navigationController?.navigationBar.addSubview(lineView2)
        
        myWaysButton = UIButton(frame: CGRect(x: (self.navigationController?.navigationBar.frame.width)!/2 - 10, y: 12, width: 100, height: 20))
        myWaysButton.setTitle("Мои маршруты", for: UIControlState())
        myWaysButton.setTitleColor(UIColor.black, for: UIControlState())
        myWaysButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 13)
        myWaysButton.addTarget(self, action: #selector(myWaysButtonClicked(_ :)), for: UIControlEvents.touchUpInside)
        self.navigationController?.navigationController?.navigationBar.addSubview(myWaysButton)
        lineView2.isHidden = true
    }

    func mapButtonClicked(_ sender:UIButton){
        lineView1.isHidden = false
        lineView2.isHidden = true
    }
    
    func myWaysButtonClicked(_ sender:UIButton){
        lineView2.isHidden = false
        lineView1.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {

        lineView2.isHidden = true
        lineView1.isHidden = false
        myWaysButton.isHidden = false
        mapButton.isHidden = false
    }
    override func viewWillDisappear(_ animated: Bool) {

        lineView1.isHidden = true
        lineView2.isHidden = true
        mapButton.isHidden = true
        myWaysButton.isHidden = true

    }

    func clearNavigationBar(){
        
        lineView1.isHidden = true
        lineView2.isHidden = true
        mapButton.isHidden = true
        myWaysButton.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
