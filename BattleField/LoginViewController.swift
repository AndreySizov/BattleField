//
//  LoginViewController.swift
//  BattleField
//
//  Created by Андрей on 11.05.17.
//
//

import UIKit
import AVFoundation

class LoginViewController: UIViewController,UITextFieldDelegate,AVCaptureMetadataOutputObjectsDelegate {

    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var idPassword: UITextField!
    @IBOutlet weak var orLabel: UILabel!
    @IBOutlet weak var qrButton: UIButton!
    @IBOutlet weak var developerLabel: UILabel!
    @IBOutlet weak var enterIdButton: UIButton!
    var input:AVCaptureDeviceInput?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //градиент
        let colorTop = UIColor(red: 105.0/255.0, green: 153.0/255.0, blue: 93.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 50.0/255.0, green: 52.0/255.0, blue: 50.0/255.0, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = self.view.bounds
        self.view.layer.insertSublayer(gradientLayer, at: 0)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.tintColor = UIColor.white
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
        
        enterIdButton.frame.size = (UIImage(named: "next")?.size)!
        enterIdButton.setImage(UIImage(named: "next"), for: UIControlState())
        enterIdButton.tintColor = UIColor.white
        enterIdButton.isHidden = true
        
        infoLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 23)
        infoLabel.textColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1.0)
    
        developerLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 13)
        developerLabel.textColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1.0)

        
        orLabel.textColor = UIColor.white
        qrButton.setTitleColor(UIColor.white, for: UIControlState())
        idPassword.textColor = UIColor.white
        
        if (self.view.frame.size.width < 375){
            
            orLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 13)
            qrButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Thin", size: 18)
            idPassword.font = UIFont(name: "HelveticaNeue-Thin", size: 18)
            
        }else{
        
        orLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 18)
        qrButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Thin", size: 25)
        idPassword.font = UIFont(name: "HelveticaNeue-Thin", size: 25)

        }
        
        qrButton.layer.borderWidth = 0.8
        qrButton.layer.borderColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1.0).cgColor
        
        
        idPassword.delegate = self
        idPassword.layer.borderWidth = 0.8
        idPassword.layer.borderColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1.0).cgColor
        idPassword.tintColor = UIColor.white
        idPassword.attributedPlaceholder = NSAttributedString(string: "Введите ID", attributes:[NSForegroundColorAttributeName: UIColor.white])
        
        var tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tap)
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        enterIdButton.isHidden = false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField.text?.isEmpty)!{
            enterIdButton.isHidden = true
        }
    }
    
    
    func hideKeyboard(){

        self.view.endEditing(true)
    }
    
    @IBAction func sendIdPassword(_ sender: Any) {
        self.view.endEditing(true)
        performSegue(withIdentifier: "showInfoVC", sender: sender)
    }
    @IBAction func sendQRCode(_ sender: Any) {
        captureQRCode()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = ""
        super.viewWillAppear(true)
    }
    //чтение qr кода
    func captureQRCode() {
        let session = AVCaptureSession()
        let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        do{
            input = try AVCaptureDeviceInput(device: device) as AVCaptureDeviceInput
            }
        catch {
            NSLog("\(error)")
            return
        }
        session.addInput(input)
        
        let output = AVCaptureMetadataOutput()
        output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        session.addOutput(output)
        output.metadataObjectTypes = [AVMetadataObjectTypeQRCode]
        
        let previewLayer = AVCaptureVideoPreviewLayer(session: session)
        let bounds = self.view.layer.bounds
        previewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        previewLayer?.bounds = bounds
        previewLayer?.position = CGPoint(x: bounds.midX, y: bounds.midY)
        
        self.view.layer.addSublayer(previewLayer!)
        session.startRunning()
    }
    
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
        for item in metadataObjects {
            if let metadataObject = item as? AVMetadataMachineReadableCodeObject {
                if metadataObject.type == AVMetadataObjectTypeQRCode {
                    NSLog("QR Code: \(metadataObject.stringValue)")
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
