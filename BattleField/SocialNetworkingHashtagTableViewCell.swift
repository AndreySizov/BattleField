//
//  SocialNetworkingHashtagTableViewCell.swift
//  BattleField
//
//  Created by Андрей on 12.05.17.
//
//

import UIKit

class SocialNetworkingHashtagTableViewCell: UITableViewCell {

    @IBOutlet weak var hashtagLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()

        hashtagLabel.font = UIFont(name: "Helvetica", size: 25)
        hashtagLabel.textColor = UIColor(red: 105.0/255.0, green: 153.0/255.0, blue: 93.0/255.0, alpha: 1.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
