//
//  ScheduleTableViewCell.swift
//  BattleField
//
//  Created by Андрей on 12.05.17.
//
//

import UIKit

class ScheduleTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var circleImageView: UIImageView!
    @IBOutlet weak var lineImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()

        titleLabel.font = UIFont(name: "Helvetica", size: 25)
        titleLabel.textColor = UIColor.black
        
        timeLabel.font = UIFont(name: "Helvetica", size: 18)
        timeLabel.textColor = UIColor(red: 109.0/255.0, green: 156.0/255.0, blue: 98.0/255.0, alpha: 1.0)
        
        infoLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 16)
        infoLabel.textColor = UIColor(red: 50.0/255.0, green: 52.0/255.0, blue: 50.0/255.0, alpha: 1.0)
        
        circleImageView.image = UIImage(named: "circle")
        lineImageView.image = UIImage(named: "verticalLine")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
