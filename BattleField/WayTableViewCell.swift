//
//  WayTableViewCell.swift
//  BattleField
//
//  Created by Андрей on 12.05.17.
//
//

import UIKit

class WayTableViewCell: UITableViewCell {

    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkButton: UIButton!
    var bool:Bool!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        titleLabel.font = UIFont(name: "Helvetica", size: 25)
        titleLabel.textColor = UIColor.black
        
        
        infoLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 16)
        infoLabel.textColor = UIColor(red: 50.0/255.0, green: 52.0/255.0, blue: 50.0/255.0, alpha: 1.0)
        
        checkButton.layer.borderWidth = 1
        checkButton.layer.borderColor = UIColor(red: 105.0/255.0, green: 153.0/255.0, blue: 93.0/255.0, alpha: 1.0).cgColor
        checkButton.layer.cornerRadius = checkButton.frame.size.height / 3
        checkButton.backgroundColor = UIColor.clear
        
        checkButton.setImage(UIImage(named: "plus"), for: UIControlState.normal)
        checkButton.tintColor = UIColor(red: 105.0/255.0, green: 153.0/255.0, blue: 93.0/255.0, alpha: 1.0)
        checkButton.imageView?.contentMode = .scaleAspectFit
        bool = false
        
    }
    @IBAction func addWay(_ sender: UIButton) {
        if (bool == false){
        checkButton.backgroundColor = UIColor(red: 105.0/255.0, green: 153.0/255.0, blue: 93.0/255.0, alpha: 1.0)
        
        checkButton.setImage(UIImage(named: "tick"), for: UIControlState.normal)
        checkButton.tintColor = UIColor.white
            checkButton.imageView?.contentMode = .scaleAspectFit
            bool = true
        }else{
            checkButton.backgroundColor = UIColor.clear
            
            checkButton.setImage(UIImage(named: "plus"), for: UIControlState.normal)
            checkButton.tintColor = UIColor(red: 105.0/255.0, green: 153.0/255.0, blue: 93.0/255.0, alpha: 1.0)
            checkButton.imageView?.contentMode = .scaleAspectFit
            bool = false
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
