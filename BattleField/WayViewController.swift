//
//  WayViewController.swift
//  BattleField
//
//  Created by Андрей on 18.05.17.
//
//

import UIKit

class WayViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {


    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var locationsTableView: UITableView!
    @IBOutlet weak var numberOfLocationsLabel: UILabel!
    var bool:Bool!
    var newBackButton:UIButton!
    var views:[UIView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.hidesBackButton = true
        newBackButton = UIButton(frame: CGRect(x: 8, y: 6, width: 41, height: 30))
        newBackButton.contentHorizontalAlignment = .left
        let imageBack = UIImage(named: "back")?.withRenderingMode(.alwaysTemplate)
        newBackButton.setImage(imageBack, for: .normal)
        newBackButton.tintColor = UIColor(red: 105.0/255.0, green: 153.0/255.0, blue: 93.0/255.0, alpha: 1.0)
        newBackButton.addTarget(self, action: #selector(WayViewController.back(sender:)), for: UIControlEvents.touchUpInside)
        self.navigationController?.navigationController?.navigationBar.addSubview(newBackButton)
        
        titleLabel.font = UIFont(name: "Helvetica", size: 30)
        titleLabel.textColor = UIColor(red: 105.0/255.0, green: 153.0/255.0, blue: 93.0/255.0, alpha: 1.0)
        
        
        infoLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 16)
        infoLabel.textColor = UIColor(red: 50.0/255.0, green: 52.0/255.0, blue: 50.0/255.0, alpha: 1.0)
        
        numberOfLocationsLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 13)
        numberOfLocationsLabel.textColor = UIColor(red: 185.0/255.0, green: 187.0/255.0, blue: 183.0/255.0, alpha: 1.0)
        
        locationsTableView.delegate = self
        locationsTableView.dataSource = self
        locationsTableView.separatorColor = UIColor.clear
        
    }
    override func viewWillAppear(_ animated: Bool) {
        views = self.navigationController?.navigationBar.subviews
        for view in (self.navigationController?.navigationBar.subviews)!{
            view.removeFromSuperview()
        }
        bool = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if(bool == false){
        self.newBackButton.removeFromSuperview()
        }
        for view in (views)!{
            self.navigationController?.navigationBar.addSubview(view)
        }
    }
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 10
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            let cell = tableView.dequeueReusableCell(withIdentifier: "wayLocations", for: indexPath)
            return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        bool = true
        performSegue(withIdentifier: "showLocationViewController", sender: nil)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
