//
//  TabBarController.swift
//  BattleField
//
//  Created by Андрей on 12.05.17.
//
//

import UIKit

class TabBarController: UITabBarController {


    var profileView: UIView!
    var bool:Bool!
    var notificationButton: UIButton!
    var gradesButton: UIButton!
    var aboutBattleButton: UIButton!
    var logOutButton: UIButton!
    var views:[UIView]!
    var numberOfNotificationsLabel:UILabel!
    var qrImageButton:UIButton!
    var idLabel:UILabel!
    var bigQRView:UIView!
    var bigQRImageView:UIImageView!
    var okButton:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()


        self.view.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.tintColor = UIColor.black
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default

        self.selectedIndex = 2
        self.tabBar.tintColor = UIColor(red: 105.0/255.0, green: 153.0/255.0, blue: 93.0/255.0, alpha: 1.0)
        var barButtonItem = UIBarButtonItem(image: UIImage(named: "person"), style: UIBarButtonItemStyle.done, target: self, action: #selector(openProfile))
        self.navigationItem.rightBarButtonItem = barButtonItem
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.gray
        
        //создание окна профиля
        let profileFrame : CGRect = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 250)
        profileView = UIView(frame: profileFrame)
        profileView.backgroundColor = UIColor(red: 105.0/255.0, green: 153.0/255.0, blue: 93.0/255.0, alpha: 1.0)
        profileView.isHidden = true
        bool = true
        
        var lineImage = UIImage(named: "line")
        var lineImageView = UIImageView(frame: CGRect(x: 0, y: 60, width: self.view.frame.size.width, height: 8))
        lineImageView.image = lineImage
        
        qrImageButton =  UIButton(frame: CGRect(x: 25, y: 6, width: 30, height: 30))
        let imageqr = UIImage(named: "qr")?.withRenderingMode(.alwaysTemplate)
        qrImageButton.setImage(imageqr, for: .normal)
        qrImageButton.tintColor = UIColor.white
        
        idLabel = UILabel(frame: CGRect(x: 75, y: 25, width: 150, height: 30))
        idLabel.text = "ID 123456789"
        idLabel.textColor = UIColor.white
        idLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 20)

        
        notificationButton = UIButton(type: UIButtonType.system)
        notificationButton.clipsToBounds = true
        profileView.isUserInteractionEnabled = true
        notificationButton.isEnabled = true
        notificationButton.isUserInteractionEnabled = true
        
        
        notificationButton = UIButton(frame: CGRect(x: 20, y: 85, width: 200, height: 30))
        gradesButton = UIButton(frame: CGRect(x: 20, y: 123, width: 200, height: 30))
        aboutBattleButton = UIButton(frame: CGRect(x: 20, y: 161, width: 200, height: 30))
        logOutButton = UIButton(frame: CGRect(x: 20, y: 199, width: 200, height: 30))
        numberOfNotificationsLabel = UILabel(frame: CGRect(x: self.view.frame.size.width - 65, y: 95, width: 40, height: 20))
        
        notificationButton.setTitle("- Уведомления", for: UIControlState())
        gradesButton.setTitle("- Награды", for: UIControlState())
        aboutBattleButton.setTitle("- О мероприятии", for: UIControlState())
        logOutButton.setTitle("- Выйти", for: UIControlState())
        numberOfNotificationsLabel.text = "0"
        
        
        notificationButton.contentHorizontalAlignment = .left
        gradesButton.contentHorizontalAlignment = .left
        aboutBattleButton.contentHorizontalAlignment = .left
        logOutButton.contentHorizontalAlignment = .left
        numberOfNotificationsLabel.textAlignment = .center

        
        notificationButton.setTitleColor(UIColor.white, for: UIControlState())
        gradesButton.setTitleColor(UIColor.white, for: UIControlState())
        aboutBattleButton.setTitleColor(UIColor.white, for: UIControlState())
        logOutButton.setTitleColor(UIColor.white, for: UIControlState())
        numberOfNotificationsLabel.textColor = UIColor.white
        
        notificationButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Thin", size: 20)
        gradesButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Thin", size: 20)
        aboutBattleButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Thin", size: 20)
        logOutButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Thin", size: 20)
        numberOfNotificationsLabel.font = UIFont(name: "Helvetica", size: 13)
        
        
        notificationButton.addTarget(self, action: #selector(notificationButtonClicked(_ :)), for: UIControlEvents.touchUpInside)
        gradesButton.addTarget(self, action: #selector(gradesButtonClicked(_ :)), for: UIControlEvents.touchUpInside)
        aboutBattleButton.addTarget(self, action: #selector(aboutBattleButtonClicked(_ :)), for: UIControlEvents.touchUpInside)
        logOutButton.addTarget(self, action: #selector(logOutButtonClicked(_ :)), for: UIControlEvents.touchUpInside)
        qrImageButton.addTarget(self, action: #selector(qrImageButtonClicked(_ :)), for: UIControlEvents.touchUpInside)
        
        numberOfNotificationsLabel.layer.borderWidth = 1
        numberOfNotificationsLabel.layer.borderColor = UIColor.white.cgColor
        numberOfNotificationsLabel.layer.cornerRadius = numberOfNotificationsLabel.frame.size.height / 2
        
        
        
        profileView.addSubview(notificationButton)
        profileView.addSubview(gradesButton)
        profileView.addSubview(aboutBattleButton)
        profileView.addSubview(logOutButton)
        profileView.addSubview(lineImageView)
        profileView.addSubview(numberOfNotificationsLabel)
        profileView.addSubview(idLabel)
        self.view.addSubview(profileView)
        
        bigQRView = UIView(frame: CGRect(x: 25, y: self.view.frame.size.height/2 - self.view.frame.size.width/2 , width: self.view.frame.size.width - 50, height: self.view.frame.size.width - 25))
        bigQRView.backgroundColor = UIColor.white
        bigQRView.layer.cornerRadius = bigQRView.frame.size.height/10
        
        okButton = UIButton(frame: CGRect(x: 5, y: bigQRView.frame.size.height - 50, width: bigQRView.frame.size.width - 10, height: 50))
        okButton.setTitle("OK", for: UIControlState())
        okButton.setTitleColor(UIColor(red: 105.0/255.0, green: 153.0/255.0, blue: 93.0/255.0, alpha: 1.0), for: UIControlState())
        okButton.titleLabel?.font = UIFont(name: "Helvetica", size: 25)
        okButton.contentHorizontalAlignment = .center
        okButton.addTarget(self, action: #selector(okButtonClicked(_ :)), for: UIControlEvents.touchUpInside)
        
        bigQRImageView = UIImageView(frame: CGRect(x: 25, y: 25, width: bigQRView.frame.size.width - 50, height: bigQRView.frame.size.width - 50))
        bigQRImageView.image = UIImage(named: "qr")
        bigQRView.addSubview(okButton)
        bigQRView.addSubview(bigQRImageView)
        self.view.addSubview(bigQRView)
        bigQRView.isHidden = true
        
    }
    
    func openProfile(){
        if(bool == true){
        profileView.isHidden = false
            bool = false
            views = self.navigationController?.navigationBar.subviews
            for view in (self.navigationController?.navigationBar.subviews)!{
                view.removeFromSuperview()
            }
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = true
            self.navigationController?.navigationBar.tintColor = UIColor.white
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
            UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
            self.navigationController?.navigationBar.addSubview(qrImageButton)
            self.qrImageButton.isHidden = false
        }else{
        profileView.isHidden = true
            bool = true
            for view in (views)!{
                    self.navigationController?.navigationBar.addSubview(view)
            }
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = true
            self.navigationController?.navigationBar.tintColor = UIColor.black
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor.gray
            UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
            self.qrImageButton.isHidden = true
        }
    }

    
    @IBAction func notificationButtonClicked(_ sender:UIButton){
        self.bigQRView.isHidden = true
        self.performSegue(withIdentifier: "showNotificationsVC", sender: sender)
        
    }
    
    func gradesButtonClicked(_ sender:UIButton){
        self.bigQRView.isHidden = true
        self.performSegue(withIdentifier: "showGradesVC", sender: sender)
    }
    
    func aboutBattleButtonClicked(_ sender:UIButton){
        self.bigQRView.isHidden = true
        self.performSegue(withIdentifier: "showInfoAboutBattleVC", sender: sender)
    }
    
    func logOutButtonClicked(_ sender:UIButton){

    }
    
    func qrImageButtonClicked(_ sender:UIButton){
        self.bigQRView.isHidden = false
    }

    func okButtonClicked(_ sender:UIButton){
        self.bigQRView.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
