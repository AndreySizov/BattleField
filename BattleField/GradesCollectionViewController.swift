//
//  GradesCollectionViewController.swift
//  BattleField
//
//  Created by Андрей on 17.05.17.
//
//

import UIKit


class GradesCollectionViewController: UICollectionViewController {

    
    var lineView:UIImageView!
    var views:[UIView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(image: UIImage(named: "back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(NotificationsTableViewController.back(sender:)))
        
        self.navigationItem.leftBarButtonItem = newBackButton
        
        let lineFrame = CGRect(x: (self.navigationController?.navigationBar.frame.width)!/2 - 10, y: 30, width: 20, height: 8)
        
        let lineImage = UIImage(named: "greenUnderline")
        lineView = UIImageView(frame: lineFrame)
        lineView.image = lineImage
        self.navigationController?.navigationBar.addSubview(lineView)
        
        self.navigationController?.navigationBar.tintColor = UIColor(red: 105.0/255.0, green: 153.0/255.0, blue: 93.0/255.0, alpha: 1.0)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
        self.navigationItem.title = "Награды"
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 13.0)!,NSForegroundColorAttributeName: UIColor.black]
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func back(sender: UIBarButtonItem) {
        self.lineView.isHidden = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
        _ = navigationController?.popViewController(animated: true)
    }
    
    
    
    func changeStyle(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named:"square"), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return 6
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "grade", for: indexPath) as! GradesCollectionViewCell
        // Configure the cell
        cell.gradeTitleLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 20)
        cell.gradeTitleLabel.textColor = UIColor.black
        
        cell.gradeImage.image = UIImage(named: "grade")
        return cell
    }

    override func viewWillAppear(_ animated: Bool) {
        views = self.navigationController?.navigationBar.subviews
        for view in (self.navigationController?.navigationBar.subviews)!{
            view.removeFromSuperview()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        for view in (views)!{
            self.navigationController?.navigationBar.addSubview(view)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.changeStyle()
    }
    
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
