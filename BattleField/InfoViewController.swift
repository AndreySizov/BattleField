//
//  InfoViewController.swift
//  BattleField
//
//  Created by Андрей on 12.05.17.
//
//

import UIKit

class InfoViewController: UIViewController {

    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var coordinatesLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var developerLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        //градиент
        let colorTop = UIColor(red: 105.0/255.0, green: 153.0/255.0, blue: 93.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 50.0/255.0, green: 52.0/255.0, blue: 50.0/255.0, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = self.view.bounds
        self.view.layer.insertSublayer(gradientLayer, at: 0)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.tintColor = UIColor.white
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
        
        
        developerLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 13)
        developerLabel.textColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1.0)
        
        infoLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 23)
        infoLabel.textColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1.0)
        
        coordinatesLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 23)
        coordinatesLabel.textColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1.0)
        
        mainLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 30)
        mainLabel.textColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1.0)
        
        nextButton.setTitleColor(UIColor.white, for: UIControlState())

        if (self.view.frame.size.width < 375){
            nextButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Thin", size: 25)
        }else{
            nextButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Thin", size: 30)
        }
    }
    
    @IBAction func goToMap(_ sender: UIButton) {
        performSegue(withIdentifier: "showMap", sender: sender)
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
