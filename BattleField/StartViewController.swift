//
//  ViewController.swift
//  BattleField
//
//  Created by Андрей on 11.05.17.
//
//

import UIKit

class StartViewController: UIViewController {

    
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var developerLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //градиент
        let colorTop = UIColor(red: 105.0/255.0, green: 153.0/255.0, blue: 93.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 50.0/255.0, green: 52.0/255.0, blue: 50.0/255.0, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = self.view.bounds
        self.view.layer.insertSublayer(gradientLayer, at: 0)
        //настройка цвета
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
        
    
        logo.image = UIImage(named: "logo")
        logo.contentMode = .scaleAspectFit
        
        welcomeLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 35)
        welcomeLabel.textColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1.0)
        
        infoLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 23)
        infoLabel.textColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1.0)
        
        developerLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 13)
        developerLabel.textColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1.0)
        
        startButton.setTitleColor(UIColor.white, for: UIControlState())
        NSLog("\(self.view.frame.size.width)")
        if (self.view.frame.size.width < 375){
            startButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Thin", size: 25)
        }else{
            startButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Thin", size: 30)
        }
        
        
        
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.navigationItem.title = ""
        super.viewWillDisappear(true)
    }
    
    @IBAction func startWork(_ sender: UIButton) {
        performSegue(withIdentifier: "showLoginVC", sender: sender)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

