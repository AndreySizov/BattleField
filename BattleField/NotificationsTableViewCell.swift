//
//  NotificationsTableViewCell.swift
//  BattleField
//
//  Created by Андрей on 16.05.17.
//
//

import UIKit

class NotificationsTableViewCell: UITableViewCell {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var whenMessageComesLabel: UILabel!
    @IBOutlet weak var messageTextLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        timeLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 35)
        timeLabel.textColor = UIColor(red: 105.0/255.0, green: 153.0/255.0, blue: 93.0/255.0, alpha: 1.0)
        
        
        messageTextLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 16)
        messageTextLabel.textColor = UIColor(red: 50.0/255.0, green: 52.0/255.0, blue: 50.0/255.0, alpha: 1.0)
        
        whenMessageComesLabel.font = UIFont(name: "Helvetica", size: 13)
        whenMessageComesLabel.textColor = UIColor(red: 50.0/255.0, green: 52.0/255.0, blue: 50.0/255.0, alpha: 1.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
