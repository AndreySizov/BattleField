//
//  WayThemeTableViewCell.swift
//  BattleField
//
//  Created by Андрей on 12.05.17.
//
//

import UIKit

class WayThemeTableViewCell: UITableViewCell {

    @IBOutlet weak var themeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()

        themeLabel.font = UIFont(name: "Helvetica", size: 30)
        themeLabel.textColor = UIColor(red: 105.0/255.0, green: 153.0/255.0, blue: 93.0/255.0, alpha: 1.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
