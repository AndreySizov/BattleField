//
//  GradesCollectionViewCell.swift
//  BattleField
//
//  Created by Андрей on 17.05.17.
//
//

import UIKit

class GradesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var gradeImage: UIImageView!
    
    @IBOutlet weak var gradeTitleLabel: UILabel!

}
