//
//  WayLocationsTableViewCell.swift
//  BattleField
//
//  Created by Андрей on 19.05.17.
//
//

import UIKit

class WayLocationsTableViewCell: UITableViewCell {

    @IBOutlet weak var circleImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var lineImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()

        titleLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 16)
        titleLabel.textColor = UIColor(red: 50.0/255.0, green: 52.0/255.0, blue: 50.0/255.0, alpha: 1.0)
        
        circleImageView.image = UIImage(named: "circle")
        lineImageView.image = UIImage(named: "verticalLine")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
